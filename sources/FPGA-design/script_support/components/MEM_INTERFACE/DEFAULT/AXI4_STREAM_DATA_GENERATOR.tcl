# Exporting core AXI4_STREAM_DATA_GENERATOR to TCL
# Exporting Create HDL core command for module AXI4_STREAM_DATA_GENERATOR
create_hdl_core -file {hdl/AXI4_STREAM_DATA_GENERATOR.v} -module {AXI4_STREAM_DATA_GENERATOR} -library {work} -package {}
# Exporting BIF information of  HDL core command for module AXI4_STREAM_DATA_GENERATOR
hdl_core_add_bif -hdl_core_name {AXI4_STREAM_DATA_GENERATOR} -bif_definition {APB:AMBA:AMBA2:slave} -bif_name {APB_TARGET} -signal_map {\
"PADDR:PADDR" \
"PENABLE:PENABLE" \
"PWRITE:PWRITE" \
"PRDATA:PRDATA" \
"PWDATA:PWDATA" \
"PREADY:PREADY" \
"PSLVERR:PSLVERR" \
"PSELx:PSEL" }
